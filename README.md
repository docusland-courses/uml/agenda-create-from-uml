# Objectifs
Lors de cet atelier, l’apprenant doit :  
-  A partir des diagrammes de classes et des diagrammes de composants d’une étude  de cas donnée par l’intervenant, développer et tester les composants de la couche  métier d’une application informatique.

## Sujet
A partir du diagramme de classe suivant , implémentez dans le langage de votre choix la structure logicielle correspondante.

## Cahier des charges:

Nous désirons implémenter la gestion d’un agenda:
- un agenda contient un ensemble de personnes nommées `contacts`.
- Un agenda possède un `owner` possédant un login/mot de passe et jwt token.
- Chaque contact est identifié par son nom et par un ensemble de coordonnées
- Une coordonnée peut être postale, téléphonique ou électonique (email ou page web)
- Chaque coordonnées possède une méthode propre permettant de valider le format de saisie. 
- Un contact doit pouvoir être exporté. Celà exportera l'ensemble des coordonées associées à ce contact. 
- Il n'est pas attendu de pouvoir exporter l'intégralité d'un agenda.

Votre application doit pouvoir contenir plusieurs agendas liés à un compte utilisateur. 
Plusieurs utilisateurs peuvent utiliser simultanément l'application. 
Ainsi un utilisateur donné ne doit pas pouvoir voir les contacts d'un autre compte utilisateur.
Il n'est pas attendu d'avoir des contacts partagés entre utilisateurs. 


## Les Use Cases associés
```plantuml
@startuml
left to right direction
actor ":Visitor" as v
actor ":User" as u
rectangle "User can connect" {
  usecase "Connect" as UC1
  usecase "Suscribe" as UC3
  usecase "Disconnect" as UC2
  usecase "Unsuscribe" as UC4
}
v --> UC1
u --> UC2
v --> UC3
u --> UC4
@enduml
```

```plantuml
@startuml
top to bottom direction
actor ":User" as u
rectangle "User can add a contact" {

  usecase "Add a phone number" as UC3
  usecase "Add an email" as UC2
  usecase "Add a new contact" as UC1
  usecase "Add an address" as UC4
  usecase "Add an website" as UC5
  usecase "Validate all contact details" as UC6
}
UC4 .> UC1 : extends 
UC5 .> UC1 : extends
UC1 <. UC2 : extends 
UC1 <. UC3 : extends 
UC1 .> UC6 : includes
u --> UC1


@enduml
```

```plantuml
@startuml
top to bottom direction
actor ":User" as u
rectangle "User can view contacts" {
  usecase "Choose an agenda" as UC0
  usecase "View a contact" as UC2
  usecase "View all contacts" as UC1
  usecase "Export the contact" as UC3
  usecase "View the ContactDetails" as UC4
}
UC1 -- UC2
UC2 <. UC3 : extends 
u --> UC0
UC0 -- UC1
UC4 <. UC2 : includes 

@enduml
```

```plantuml
@startuml
top to bottom direction
actor ":User" as u
rectangle "User can modify a contact" {

  usecase "Add a ContactDetail" as UC3
  usecase "Edit a ContactDetail" as UC2
  usecase "Edit a contact" as UC1
  usecase "Remove a ContactDetail" as UC4
  usecase "Validate all contact details" as UC6
}
UC4 .> UC1 : extends 
UC2 .> UC1 : extends 
UC1 <. UC3 : extends 
UC1 .> UC6 : includes
u --> UC1
@enduml
```

```plantuml
@startuml
top to bottom direction
actor ":User" as u
rectangle "User can delete records" {
  usecase "Delete an agenda" as UC1
  usecase "Delete a Contact" as UC2
  usecase "Delete a ContactDetail" as UC3
}
UC1 .> UC2 : includes 
UC2 .> UC3 : includes 

u --> UC1
u --> UC2
u --> UC3
@enduml
```

## Les Diagramme de classe souhaité

```mermaid
classDiagram
    User "1" *-- "0..*" Agenda
    Agenda "1" *-- "0..*" Contact
    Contact "1" *-- "0..*" ContactDetail
    ContactDetail <|-- Address
    ContactDetail <|-- Phone
    ContactDetail <|-- Email
    ContactDetail <|-- Website
    <<abstract>> ContactDetail
    class Agenda{
        +String label
    }
    class Contact{
        +String name
    }
    class ContactDetail {
        +String value
        #String pattern
        +validate()

    }
    class User{
      +String login
      -String password
      -String jwt
    }
    class Address{
        #String pattern
    }
    class Phone{
        #String pattern
    }
    class Email{
        #String pattern
    }
    class Website{
        #String pattern
    }
```
Il se peut que votre implémentation finale ne respecte pas à la lettre ce diagramme de classe. ( En fonction de la technologie choisie typiquement... Ce n'est pas grave mais tâchez de respecter au mieux ce modèle fourni)

## Attendus de l'exercice
Dans le langage de votre choix, implémentez les classes liées à ce diagramme.

Vérifiez l'ensemble des méthodes nécessaires pour que l'agenda soit opérationnel.
Que ce soit par des tests ou par un script `main` basique, par exemple : 
```python
thomas = Contact("Thomas", "Le Pipeau")
thomas.addContactDetail(Phone("01 23 45 67 89"))
thomas.addContactDetail(Email("thomas.lepipeau@lilo.org"))

try:
   thomas.addContactDetail(Phone("Un mauvais num"))
except InvalidValueException:
   print("Thomas contact is properly set")
   print(thomas)
else:
   raise Exception("Invalid phone number")

marcel = User(login="Marcel", password="Orchestre")
personnalAgenda = Agenda("Personnel") 
marcel.addAgenda(personnalAgenda)

personnalAgenda.addContact(thomas)
print("Marcel has a new contact named Thomas")
print(marcel)

# ... Continue with getContacts, getContact, setValue, deleteContact ...
```

A partir du code produit, réalisez le diagramme de classe final.
Vous pouvez utiliser des pattern (Factory, MVC, MVVT ...).

## Les consignes ci-dessus sont le minimum attendu
Vous avez des niveaux variés, et les exigences ci-dessus sont minimalistes au vu du temps imparti : 7 heures...
Le travail fourni doit être propre et structuré.
Définissez le scope en amont, il est possible de réaliser les fonctionnalités suivantes en bonus :
 - Implémenter des tests unitaires ou fonctionnels au besoin.
 - Persister les données.
 - Ajouter une api
 - Implémenter un client communiquant avec un back-end simulé.

 

## Livrables attendus
Livraison par le biais d'un dépôt git communiqué au formateur contenant :
 - un fichier README.md expliquant à minima (2pts):
    - comment installer lancer le projet .
    - comment lancer l'executable de votre projet.
    - comment lancer un environnement de dev.
    - un screenshot ou plusieurs de l'application lancée.
 - le code source de l'application (9/10pts)
 - le code compilé s'il s'agit d'un langage compilé (0/1pts)
 - le diagramme de classe final. Non pas de vos modèles mais de la structure mise en place. (4pts)

Votre note sera sur 16 pts si vous vous contentez du minimum attendu. 
Les bonus pouvant vous apporter des points supplémentaires.
